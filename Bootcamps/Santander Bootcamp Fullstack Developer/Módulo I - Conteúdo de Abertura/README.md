# Criando seu Primeiro Repositório no GitHub Para Compartilhar Seu Progresso.

## Links úteis
[Sintaxe básica Markdown](https://www.markdownguide.org/)

[Documentação do Git](https://git-scm.com/doc)

[Manual do usuário Git](https://drive.google.com/file/d/1gUoR2U3FCWbDRvqS4BO2Hc93WUqkiUzI/view?usp=sharing)

[Conheça a plataforma da DIO](https://www.dio.me/)

[Comunidade DIO](https://discord.gg/qaFMgEKw)
